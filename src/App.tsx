// https://babeljs.io/docs/en/babel-polyfill
import 'core-js/stable';
import 'regenerator-runtime/runtime';
// https://babeljs.io/docs/en/next/babel-plugin-syntax-dynamic-import.html#working-with-webpack-and-babel-preset-env
import 'core-js/modules/es.promise';
import 'core-js/modules/es.array.iterator';
// https://meyerweb.com/eric/tools/css/reset/
import 'reset-css';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {
  BrowserRouter, Switch, Route, Redirect,
} from 'react-router-dom';
import { Provider, useSelector } from 'react-redux';

import { getUserAddress } from '@/app/user/redux/selectors';
import routes from './app/base/constants/routes';
import Home from './pages/Home';
import Login from './pages/Login';
import store from './app/base/redux';

import '@/style/base.scss';

function Routes(): JSX.Element {
  const userAddress = useSelector(getUserAddress);

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path={routes.home}>
          {userAddress ? <Home /> : <Redirect to={routes.login} />}
        </Route>
        <Route exact path={routes.login}>
          {userAddress ? <Redirect to={routes.home} /> : <Login />}
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

function App(): JSX.Element {
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
}

ReactDOM.render(<App />, document.getElementById('app'));
