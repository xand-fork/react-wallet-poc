import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

import WelcomeMessage from '@/app/user/components/WelcomeMessage';
import LoginInput from '@/app/user/components/LoginInput';

import './style.scss';

export default function Login(props: object): JSX.Element {
  return (
    <Container className="login-root">
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
      >
        <Grid item>
          <WelcomeMessage />
        </Grid>
        <Grid item className="login-btn">
          <LoginInput {...props} />
        </Grid>
      </Grid>
    </Container>
  );
}
