import React from 'react';
import Container from '@material-ui/core/Container';

import WalletInfoSection from '@/app/xand/components/WalletInfoSection';
import BankInfoSection from '@/app/banks/components/BanksInfoSection';
import Logout from '@/app/user/components/Logout';

import './style.scss';

export default function Home(): JSX.Element {
  return (
    <div className="home-root">
      <div className="logout-container"><Logout /></div>
      <Container>
        <WalletInfoSection />
        <div className="bank-info-section"><BankInfoSection /></div>
      </Container>
    </div>
  );
}
