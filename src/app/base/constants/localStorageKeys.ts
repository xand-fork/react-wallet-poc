export default {
  address: 'address',
  memberApiUrl: 'member-api-url',
  jwtToken: 'jwt-token',
};
