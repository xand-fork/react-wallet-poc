/* eslint-disable max-len */
import localStorageKeys from '@/app/base/constants/localStorageKeys';

function getUniqueKey(userAddress: string, item: string): string {
  return `${item}-${userAddress}`;
}

const persistence = {
  getAddress: (): string | null => localStorage.getItem(localStorageKeys.address),
  setAddress: (address: string | null): void => {
    if (!address) {
      localStorage.removeItem(localStorageKeys.address);
    } else {
      localStorage.setItem(localStorageKeys.address, address);
    }
  },
  getMemberApiUrl: (userAddress: string): string | null => {
    const key = getUniqueKey(userAddress, localStorageKeys.memberApiUrl);
    return localStorage.getItem(key);
  },
  setMemberApiUrl: (userAddress: string, url: string | null): void => {
    const key = getUniqueKey(userAddress, localStorageKeys.memberApiUrl);
    if (!url) {
      localStorage.removeItem(key);
    } else {
      localStorage.setItem(key, url);
    }
  },
  getJwtToken: (userAddress: string): string | null => {
    const key = getUniqueKey(userAddress, localStorageKeys.memberApiUrl);
    return localStorage.getItem(key);
  },
  setJwtToken: (userAddress: string, token: string | undefined): void => {
    const key = getUniqueKey(userAddress, localStorageKeys.jwtToken);
    if (!token) {
      localStorage.removeItem(key);
    } else {
      localStorage.setItem(key, token);
    }
  },
  clear: (userAddress: string): void => {
    const jwtKey = getUniqueKey(userAddress, localStorageKeys.jwtToken);
    const urlKey = getUniqueKey(userAddress, localStorageKeys.memberApiUrl);
    localStorage.removeItem(jwtKey);
    localStorage.removeItem(urlKey);
    localStorage.removeItem(localStorageKeys.address);
  },
};


export default persistence;
