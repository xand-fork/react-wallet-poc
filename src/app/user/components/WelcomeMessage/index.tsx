import React from 'react';
import Typography from '@material-ui/core/Typography';
import logo from '@/images/transparent_logo.svg';

import './style.scss';


export default function WelcomeMessage(): JSX.Element {
  return (
    <div className="welcome-message-root">
      <img className="brand-logo" src={logo} alt="" />
      <Typography variant="h3" gutterBottom>
        Welcome to Xand!
      </Typography>
      <Typography variant="h5" gutterBottom>
        Moving Money Forward
      </Typography>
    </div>
  );
}
