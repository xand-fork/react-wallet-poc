/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React from 'react';
import Button from '@material-ui/core/Button';
import { useDispatch } from 'react-redux';

import { setUserAddress } from '@/app/user/redux/actions';
import { refreshBalance } from '@/app/xand/redux/actions';

export default function LoginInput(): JSX.Element {
  const dispatch = useDispatch();

  const loadAddress = (event: React.FormEvent<HTMLInputElement>): void => {
    const file = (event.target as HTMLInputElement)!.files![0];
    const reader = new FileReader();
    reader.onloadend = (e: ProgressEvent<FileReader>): void => {
      const addr = e.target!.result!.toString().replace('ADDRESS=', '').trim();
      // Note: dispatching two actions here needs to be changed to a new redux-thunk action
      // (like 'UserActionTypes.Login')
      // that encapuslated any dispatches and async calls needed during a login.
      // TODO.
      dispatch(setUserAddress(addr));
      dispatch(refreshBalance(addr));
    };
    reader.readAsText(file);
  };

  return (
    <Button
      variant="contained"
      component="label"
      color="primary"
    >
      Import User Key
      <input
        type="file"
        style={{ display: 'none' }}
        onChange={loadAddress}
      />
    </Button>
  );
}
