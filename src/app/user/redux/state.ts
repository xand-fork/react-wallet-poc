import persistence from '@/app/base/logic/persistence';

export interface UserState {
  address: string;
  jwtToken: string;
}

export const loadInitialUserState = (): UserState => {
  const address = persistence.getAddress() || '';
  const jwtToken = persistence.getJwtToken(address) || '';
  return { address, jwtToken };
};
