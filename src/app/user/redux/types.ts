enum UserActionType {
  Logout = 'logout',
  SetAddress = 'set_address',
  SetJwtToken = 'set_token',
}

export default UserActionType;
