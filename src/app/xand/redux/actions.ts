import Money from 'tpfs-money';
import { ThunkAction } from 'redux-thunk';

import { getUserBalance } from '@/app/xand/api';
import XandActionTypes from './types';
import { XandState } from './state';

export interface XandAction {
  type: XandActionTypes;
  balance?: Money;
  errorMsg?: string;
  isLoading?: boolean;
}

export const setBalance = (balance: Money): XandAction => ({
  type: XandActionTypes.SetBalance,
  balance,
});

export const setLoadingStatus = (isLoading: boolean): XandAction => ({
  type: XandActionTypes.SetBalanceLoading,
  isLoading,
});

export const setError = (errorMsg: string | undefined): XandAction => ({
  type: XandActionTypes.SetBalanceError,
  errorMsg,
});

export const refreshBalance = (address: string):
  ThunkAction<void, XandState, unknown, XandAction> => async (dispatch): Promise<void> => {
  dispatch(setLoadingStatus(true));
  dispatch(setError(''));
  try {
    const balance = await getUserBalance(address);
    const newBalance = new Money(balance.balanceInMinorUnit, 'USD');
    dispatch(setBalance(newBalance));
  } catch (ex) {
    dispatch(setError(`${ex}`));
  } finally {
    dispatch(setLoadingStatus(false));
  }
};
