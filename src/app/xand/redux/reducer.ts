import Money from 'tpfs-money';

import UserActionType from '@/app/user/redux/types';
import { UserAction } from '@/app/user/redux/actions';

import XandActionType from './types';
import { XandAction } from './actions';
import { XandState, initialState } from './state';

const xandReducer = (state: XandState = initialState, action: XandAction | UserAction):
  XandState => {
  switch (action.type) {
    case XandActionType.SetBalance: {
      return { ...state, walletBalance: action.balance || new Money(0, 'USD') };
    }
    case XandActionType.SetBalanceError: {
      return { ...state, balanceError: action.errorMsg || undefined };
    }
    case XandActionType.SetBalanceLoading: {
      return { ...state, loadingBalance: action.isLoading || false };
    }
    case UserActionType.Logout: {
      return initialState;
    }
    default: {
      return state;
    }
  }
};

export default xandReducer;
