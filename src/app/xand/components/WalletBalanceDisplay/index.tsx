import React from 'react';
import Money from 'tpfs-money';
import { Typography } from '@material-ui/core';

export interface WalletBalanceDisplayProps {
  isRefreshing: boolean;
  errorMsg?: string;
  balance: Money;
  size: 'h1' | 'h2' | 'h3' | 'h4' | 'h5';
}

export default function WalletBalanceDisplay(props: WalletBalanceDisplayProps): JSX.Element {
  const {
    balance, size, isRefreshing, errorMsg,
  } = props;
  return (
    <>
      { isRefreshing && (
        <Typography variant={size}>
          Loading Balance...
        </Typography>
      )}
      { !isRefreshing && (
        <Typography variant={size}>
          {`${balance}`}
        </Typography>
      )}
      { errorMsg && (
        <Typography variant="h6" color="error">
          {errorMsg}
        </Typography>
      )}
    </>
  );
}
