/* eslint-disable import/prefer-default-export */
import {
  WalletsApi,
  Configuration,
  XandBalance,
} from 'xand-member-api-client';

import routes from '@/app/base/constants/routes';

export const getUserBalance = async (address: string): Promise<XandBalance> => {
  const apiConfig = new Configuration({
    basePath: routes.api,
  });
  const api = new WalletsApi(apiConfig);
  return (await api.getBalance(address)).data;
};
