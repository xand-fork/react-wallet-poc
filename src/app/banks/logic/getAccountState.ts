import { BankAccount } from 'xand-member-api-client';
import Money from 'tpfs-money';

import BankAccountState from '@/app/banks/interfaces/BankAccountState';
import { getAccountBalance } from '@/app/banks/api';

export const accountToAccountState = (account: BankAccount, balance: Money): BankAccountState => ({
  ...account, errorMsg: undefined, isRefreshing: false, balance,
});

export const getAccountState = async (account: BankAccount): Promise<BankAccountState> => {
  const balance = await getAccountBalance(account.accountId || '');
  const newBalance = new Money(balance.availableBalanceInMinorUnit || 0, 'USD');
  return accountToAccountState(account, newBalance);
};
