import Money from 'tpfs-money';
import { BankAccount } from 'xand-member-api-client';

export default interface BankAccountState extends BankAccount {
  errorMsg: string | undefined;
  balance: Money;
  isRefreshing: boolean;
// eslint-disable-next-line semi
}
