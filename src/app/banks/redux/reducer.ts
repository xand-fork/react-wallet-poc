import { UserAction } from '@/app/user/redux/actions';
import UserActionType from '@/app/user/redux/types';

import BanksActionType from './types';
import { BanksAction } from './actions';
import { initialState, BanksState } from './state';
import setAccountBalance from './reducer/setAccountBalance';
import setAccountError from './reducer/setAccountError';
import setAccountIsRefreshing from './reducer/setAccountIsRefreshing';
import setAllAccounts from './reducer/setAllAccounts';
import setAccountsIsReloading from './reducer/setAccountsIsReloading';

const banksReducer = (state: BanksState = initialState, action: BanksAction | UserAction):
  BanksState => {
  switch (action.type) {
    case BanksActionType.ClearAccounts: {
      return { ...state, accounts: {} };
    }
    case BanksActionType.SetAccountBalance: {
      const { accountId, balance } = action;
      return setAccountBalance(state, accountId, balance);
    }
    case BanksActionType.SetAccountError: {
      const { accountId, errorMsg } = action;
      return setAccountError(state, accountId, errorMsg);
    }
    case BanksActionType.SetAccountIsRefreshing: {
      const { accountId, isRefreshing } = action;
      return setAccountIsRefreshing(state, accountId, isRefreshing);
    }
    case BanksActionType.SetAccountsError: {
      const { errorMsg } = action;
      return { ...state, errorMsg };
    }
    case BanksActionType.SetAccountsIsReloading: {
      const { isReloading } = action;
      return setAccountsIsReloading(state, isReloading);
    }
    case BanksActionType.SetAllAccounts: {
      const { accounts } = action;
      return setAllAccounts(state, accounts);
    }
    case UserActionType.Logout: {
      return initialState;
    }
    default: {
      return state;
    }
  }
};

export default banksReducer;
