import Money from 'tpfs-money';

import { BanksState } from '@/app/banks/redux/state';

export default function setAccountBalance(
  state: BanksState,
  accountId: string | undefined,
  balance: Money | undefined,
): BanksState {
  if (!accountId) {
    return { ...state, errorMsg: 'Account Id missing.' };
  }
  if (!balance) {
    return { ...state, errorMsg: `Balance update for ${accountId} missing balance.` };
  }
  const account = { ...state.accounts[accountId] };
  if (!Object.keys(account).length) {
    return { ...state, errorMsg: `Account ${accountId} could not be found.` };
  }
  const newState: BanksState = { ...state };
  account.balance = balance;
  newState.accounts = { ...newState.accounts, [accountId]: account };
  return newState;
}
