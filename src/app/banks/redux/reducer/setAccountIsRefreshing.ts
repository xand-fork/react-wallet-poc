import { BanksState } from '@/app/banks/redux/state';

export default function setAccountIsRefreshing(
  state: BanksState,
  accountId: string | undefined,
  isRefreshing: boolean | undefined,
): BanksState {
  if (!accountId) {
    return { ...state, errorMsg: 'Account Id missing.' };
  }
  if (isRefreshing === undefined) {
    return { ...state, errorMsg: `Refreshing status for ${accountId} missing.` };
  }
  const account = { ...state.accounts[accountId] };
  if (!Object.keys(account).length) {
    return { ...state, errorMsg: `Account ${accountId} could not be found.` };
  }
  const newState: BanksState = { ...state };
  account.isRefreshing = isRefreshing;
  newState.accounts = { ...newState.accounts, [accountId]: account };
  return newState;
}
