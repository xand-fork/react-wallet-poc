import { BanksState } from '@/app/banks/redux/state';
import BankAccountState from '@/app/banks/interfaces/BankAccountState';
import accountStatesToObject from '@/app/banks/logic/accountStatesToObject';

export default function setAllAccounts(
  state: BanksState,
  accounts: BankAccountState[] | undefined,
): BanksState {
  if (!accounts) {
    return { ...state, errorMsg: 'Missing accounts collection.' };
  }
  const accountObj = accountStatesToObject(accounts);
  return { ...state, accounts: accountObj };
}
