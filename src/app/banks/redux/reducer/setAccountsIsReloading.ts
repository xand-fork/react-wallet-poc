import { BanksState } from '@/app/banks/redux/state';

export default function setAccountsIsReloading(
  state: BanksState,
  isReloading: boolean | undefined,
): BanksState {
  if (isReloading === undefined) {
    return { ...state, errorMsg: 'Missing loading status for bank accounts.' };
  }
  return { ...state, isReloading };
}
