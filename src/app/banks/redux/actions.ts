import Money from 'tpfs-money';
import { ThunkAction } from 'redux-thunk';
import { BankAccount } from 'xand-member-api-client';

import { getAccounts, getAccountBalance } from '@/app/banks/api';
import BankAccountState from '@/app/banks/interfaces/BankAccountState';
import { getAccountState } from '@/app/banks/logic/getAccountState';

import BankActionTypes from './types';
import { BanksState } from './state';

export interface BanksAction {
  type: BankActionTypes;
  accountId?: string;
  isRefreshing?: boolean;
  isReloading?: boolean;
  errorMsg?: string | undefined;
  balance?: Money;
  accounts?: BankAccountState[];
}

export const setAccountsIsReloading = (isReloading: boolean): BanksAction => ({
  type: BankActionTypes.SetAccountsIsReloading,
  isReloading,
});

export const setAccountsError = (errorMsg: string | undefined): BanksAction => ({
  type: BankActionTypes.SetAccountsError,
  errorMsg,
});

export const setAccounts = (accounts: BankAccountState[]): BanksAction => ({
  type: BankActionTypes.SetAllAccounts,
  accounts,
});

export const clearAccounts = (): BanksAction => ({
  type: BankActionTypes.ClearAccounts,
});

export const setAccountIsRefreshing = (accountId: string, isRefreshing: boolean): BanksAction => ({
  type: BankActionTypes.SetAccountIsRefreshing,
  accountId,
  isRefreshing,
});

export const setAccountError = (accountId: string, errorMsg: string | undefined): BanksAction => ({
  type: BankActionTypes.SetAccountError,
  accountId,
  errorMsg,
});

export const setAccountBalance = (accountId: string, balance: Money): BanksAction => ({
  type: BankActionTypes.SetAccountBalance,
  accountId,
  balance,
});

export const refreshAccountBalance = (accountId: string):
  ThunkAction<void, BanksState, unknown, BanksAction> => async (dispatch): Promise<void> => {
  dispatch(setAccountError(accountId, undefined));
  dispatch(setAccountIsRefreshing(accountId, true));
  try {
    const balance = await getAccountBalance(accountId);
    const newBalance = new Money(balance.availableBalanceInMinorUnit || 0, 'USD');
    dispatch(setAccountBalance(accountId, newBalance));
  } catch (ex) {
    dispatch(setAccountError(accountId, `${ex}`));
  } finally {
    dispatch(setAccountIsRefreshing(accountId, false));
  }
};

export const refreshAllAccounts = ():
  ThunkAction<void, BanksState, unknown, BanksAction> => async (dispatch): Promise<void> => {
  dispatch(setAccountsError(undefined));
  dispatch(setAccountsIsReloading(true));
  dispatch(clearAccounts());
  try {
    const accounts = await getAccounts();
    const balancePromises: Array<Promise<BankAccountState>> = [];
    accounts.forEach((acct: BankAccount) => {
      balancePromises.push(getAccountState(acct));
    });
    const accountStates = await Promise.all(balancePromises);
    dispatch(setAccounts(accountStates));
  } catch (ex) {
    dispatch(setAccountsError(`${ex}`));
  } finally {
    dispatch(setAccountsIsReloading(false));
  }
};
