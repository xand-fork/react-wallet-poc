import BankAccountState from '@/app/banks/interfaces/BankAccountState';

export interface BanksState {
  errorMsg: string | undefined;
  isReloading: boolean;
  accounts: {[key: string]: BankAccountState};
}

export const initialState: BanksState = {
  errorMsg: undefined,
  isReloading: false,
  accounts: {},
};
