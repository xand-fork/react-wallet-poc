enum BanksActionTypes {
  SetAccountsIsReloading = 'set_accounts_is_reloading',
  SetAccountsError = 'set_accounts_error',
  SetAllAccounts = 'set_all_accounts',
  ClearAccounts = 'clear_accounts',
  SetAccountIsRefreshing = 'set_account_is_refreshing',
  SetAccountError = 'set_account_error',
  SetAccountBalance = 'set_account_balance'
}

export default BanksActionTypes;
