import React from 'react';
import { Card, Typography } from '@material-ui/core';

import BankAccountState from '@/app/banks/interfaces/BankAccountState';
import RefreshAccountButton from '@/app/banks/components/RefreshAccountButton';
import getBankInfo from '@/app/banks/logic/getBankInfo';

import './style.scss';

export interface AccountListItemProps {
  account: BankAccountState;
}

export default function AccountListItem(props: AccountListItemProps): JSX.Element {
  const { account } = props;
  const bankInfo = getBankInfo(account.routingNumber || '');
  return (
    <Card className="account-list-item-root" elevation={1}>
      {bankInfo?.logo && <img className="bank-img" alt="bank logo" src={bankInfo.logo} />}
      <Typography variant="h6">{`${account.accountName}`} - {`x${account.truncatedAccountNumber}`}</Typography>
      <Typography color="primary" variant="body1">Bal.: {`${account.balance}`}</Typography>
      <Typography variant="body1">Bank Name: {`${bankInfo?.name}`}</Typography>
      {account.errorMsg && <Typography color="error" variant="body1">Error Msg: {`${account.errorMsg}`}</Typography>}
      <Typography variant="body1">IsRefreshing: {`${account.isRefreshing}`}</Typography>
      <RefreshAccountButton accountId={account.accountId || ''} />
    </Card>
  );
}
