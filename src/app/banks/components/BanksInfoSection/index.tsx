import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { useSelector, shallowEqual } from 'react-redux';

import RefreshAllBalances from '@/app/banks/components/RefreshAllBalances';
import { getAccounts, getIsAccountsReloading, getAccountsErrorMsg } from '@/app/banks/redux/selectors';
import AccountList from '@/app/banks/components/AccountList';

import './style.scss';

export default function BankInfoSection(): JSX.Element {
  const accounts = useSelector(getAccounts, shallowEqual);
  const isAccountsReloading = useSelector(getIsAccountsReloading);
  const accountsErrorMsg = useSelector(getAccountsErrorMsg);

  return (
    <Grid
      className="banks-info-section-root banks-info banks-info-section"
      container
      direction="column"
    >
      <Grid item>
        <Typography variant="h5" className="heading">Bank/Account Information</Typography>
      </Grid>
      <Grid item>
        <RefreshAllBalances />
      </Grid>
      <Grid item>
        <Typography variant="body1">IsReloading: {JSON.stringify(isAccountsReloading)}</Typography>
        {accountsErrorMsg && <Typography variant="body1" color="error">Accounts error: {JSON.stringify(accountsErrorMsg)}</Typography>}
      </Grid>
      {
        accounts && !!Object.values(accounts).length
        && (
          <Grid item>
            <AccountList accounts={accounts} />
          </Grid>
        )
      }
    </Grid>
  );
}
