# What's This?

Small POC react website wallet.


# Current TODOs:
- remove coupled redux dispatches from components (logout clearing things etc)
- unit test examples


# Actions

- `yarn install` Install all dependencies.
- `yarn lint` - Lint the project
- `yarn fix` - Lint and autofix
- `yarn test` - Run jest tests
    - generates a coverage report to `/reports/coverage`
- `yarn build-dev` - Build to `/dist` with dev webpack settings
    - Generates a webpack analyzer report in `/reports`
- `yarn build` - Build to `/dist` with prod webpack settings
    - Automatic favicon generation
    - Generates a webpack analyzer report in `/reports`
- `yarn launch` - Starts `webpack-dev-server` and serves content to localhost:8080

# Things this does not has yet:

- styled components instead of scss https://styled-components.com/
- react-helmet for proper metadata setting https://www.npmjs.com/package/react-helmet
- any nice to haves from here: https://github.com/thedaviddias/Front-End-Checklist
- any sort of post-mvp features like oauth etc

# Project Workflow

    Typescript + React Website Code             # Website Code
    -> Babel (via `babel-loader` in `webpack`)  # "Transpiling"
    -> Webpack                                  # "Bundling"
    -> `/dist`                                  # Output

The project is written in Typescript with React.  When ready to build, webpack runs through codebase at the given `entry` and treats each imported file via its defined `loader` for its extension.  js/ts/jsx/tsx files are given to `babel-loader` where `babel` handles any code transformations it needs to make.  Files are given to `file-loader` to be properly named and placed in `/dist`.  Etc.  

# Configuration

### Typescript

- `tsconfig.json`

### Babel

- `.babelrc`
- `.browserslistrc`

### Webpack

- `webpack.config.js`
- `webpack/**/*`

### eslint

- `.eslintrc.js`
- `.eslintignore`

### Jest
- `jest.config.js`